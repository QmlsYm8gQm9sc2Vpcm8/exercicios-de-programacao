class Faculdade(object):

    def __init__(self):
        self.aluno = {"Nome":[], "Nota": [], "Media":[]}
        self.endereco = r"C:\Users\Lab1\Desktop\Alunos.txt"
        self.texto = "Aluno: {aluno}\nNota 1: {nota1}\nNota 2: {nota2}\nNota 3: {nota3}\nMédia: {media}"

    def cadastrar_nota(self, aluno):
        self.aluno["Nome"].append(aluno)
        for i in range(3):
            self.aluno["Nota"].append(eval(input("Digite a nota da prova {numero}: ".format(numero = i + 1))))
        return "Notas Cadastradas!"

    def gerar_media(self):
        self.aluno["Media"] = sum(self.aluno["Nota"])/len(self.aluno["Nota"])
        return "Média Cadastrarda"

    def ler_dados(self):
        dados = open(r'{}'.format(self.endereco), "r")
        for linha in dados:
            linha = linha.rstrip()
            print(linha)
        dados.close()

    def escrever_dados(self):
        dados = open(r'{}'.format(self.endereco), "a")
        dados.write("\n")
        dados.write(self.texto.format(aluno = self.aluno["Nome"], nota1 = self.aluno["Nota"][0], nota2 = self.aluno["Nota"][1], nota3 = self.aluno["Nota"][2], media = self.aluno["Media"]))
        dados.close()

if __name__ == '__main__':
    run = True
    ies = Faculdade()
    while(run):
        ies.cadastrar_nota(input("Digite o nome do aluno: "))
        ies.gerar_media()
        ies.escrever_dados()
        ies.ler_dados()
        break
        
