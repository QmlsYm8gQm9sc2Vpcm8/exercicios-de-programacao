from execution_time import calcula_duracao
class Exercicio03(object):

	def __init__(self, n1, n2):
		self.n1 = n1
		self.n2 = n2

	@calcula_duracao
	def somar (self):
		return self.n1 + self.n2

	@calcula_duracao
	def dividir (self):
		return self.n1 / self.n2

	@calcula_duracao
	def multiplicar (self):
		return self.n1 * self.n2

	@calcula_duracao
	def subtrair (self):
		return self.n1 - self.n2

if __name__ == '__main__':
	teste = Exercicio03(eval(input("Digite o primeiro número: ")), eval(input("Digite o segundo número: ")))
	soma = teste.somar()
	subtracao = teste.subtrair()
	multiplicar = teste.multiplicar()
	dividir = teste.dividir()
	print("Soma: {} \nSubtração: {} \nMultiplicação: {} \nDivisão: {} \n".format(soma, subtracao, multiplicar, dividir))