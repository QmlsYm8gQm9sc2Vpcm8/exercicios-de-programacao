import time

def calcula_duracao(funcao):
    def tempo(*args, **kwargs):
        arquivo = open('log.txt','a')
        t_inicial = time.time()
        print("%.2f" % t_inicial)
        funcao(*args, **kwargs)
        t_final = time.time()
        print("%.2f" % t_final)
        arquivo.write('[{funcao}] Tempo total de Execu��o: {tempo_total}\n'.format(
            funcao = funcao.__name__,
            tempo_total = "{0:.10f}".format(t_final - t_inicial)
        ))
        arquivo.close()
        return funcao(*args, **kwargs)

    try:
        arquivo = open('log.txt', 'r')
        arquivo.close()
        return tempo
    except:
        arquivo = open('log.txt', 'w')
    return tempo