import unittest
from exercicio_01 import Exercicio01

class TesteUnitarioExercicio01(unittest.TestCase):

    #Constantes
    MAIOR_IDADE = 18

    #Objeto Instanciado
    obj = Exercicio01("Josemar")
    
    def test_testar_idade(self):
        self.assertEqual(self.obj.testar_idade(self.MAIOR_IDADE), "É maior de idade")

if __name__ == "__main__":
    unittest.main()
        
