class Exercicio26(object):

	lista_ordenada = []

	def __init__ (self, lista):
		if (self.verificacao(lista) == 1):
			print(self.separacao(lista))
		else:
			print("O numero de elementos precisam ser pares maiores que zero.")

	def verificacao(self, lista):
		if len(lista) > 0:
			if (len(lista) % 2 == 0):
				return 1
			else:
				return 0
		return 0
		
	def separacao(self, lista):
		lista_1 = []
		lista_2 = []
		tam = int(len(lista)/2)
		for i in range (0, tam, 1):
			lista_1.append(lista[i])
		for i in range (tam, int(len(lista)), 1):
			lista_2.append(lista[i])
		return self.uniao(lista_1, lista_2) 
		

	def uniao(self, lista_1, lista_2):
		self.lista_ordenada = lista_2 + lista_1
		return self.lista_ordenada

if __name__ == '__main__':
	Exercicio26([4,5,6,1,2,3])