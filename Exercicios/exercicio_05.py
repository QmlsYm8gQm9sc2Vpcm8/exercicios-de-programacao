class Exercicio05(object):

	def __init__(self, valor):
		self.valor = valor

	def sinal(self):
		if self.valor >= 0:
			return "O número é positivo"
		else:
			return "O número é negativo"

if __name__ == "__main__":
	valor = Exercicio05(eval(input("Digite um número, e lhe direi se é Positivo ou Negativo: ")))
	print(valor.sinal())

