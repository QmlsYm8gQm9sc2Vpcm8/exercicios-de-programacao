class Exercicio10(object):

	def __init__(self, nota1, nota2, nota3):
		self.nota1 = nota1
		self.nota2 = nota2
		self.nota3 = nota3

	@property
	def media(self):
		return (self.nota1 + self.nota2 + self.nota3)/3

	def resultado(self, media):
		if media >= 7:
			return "Passou!"
		else:
			return "Reprovou!"

if __name__ == '__main__':
	aluno = Exercicio10(eval(input("Digite a primeira Nota: ")), eval(input("Digite a segunda Nota: ")), eval(input("Digite a terceira Nota: ")))
	print(aluno.resultado(int(aluno.media)))		
