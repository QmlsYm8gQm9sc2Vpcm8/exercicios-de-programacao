class Exercicio06(object):

    def __init__(self, n1):
        self.n1 = n1

    def sinal (self):
        if self.n1 >= 0:
            return "Positivo"
        else: 
            return "Negativo"

    def par_impar(self):
        if self.n1 % 2 == 0:
            return "Par"
        else:
            return "Impar"

if __name__ == '__main__':
    numero = Exercicio06(int(eval(input("Digite um número: "))))
    print("O número é {} e é um número {}".format(numero.sinal(), numero.par_impar()))
