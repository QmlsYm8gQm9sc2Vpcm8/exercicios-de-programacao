class Exercicio05b(object):

	def __init__(self, valor):
		self.valor = valor

	def sinal(self):
		if self.valor >= 0:
			return "O número é positivo"
		else:
			return "O número é negativo"
	def par_ou_impar(self):
		if self.valor % 2 == 0:
			return "O número é par"
		else:
			return "O número é impar"

if __name__ == "__main__":
	valor = Exercicio05b(eval(input("Digite um número, e lhe direi se é Positivo ou Negativo: ")))
	print(valor.sinal())
	print(valor.par_ou_impar())

