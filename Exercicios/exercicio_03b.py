class Exercicio03b(object):

	soma = 0
	multiplicacao = 0
	divisao = 0
	subtracao = 0

	def __init__(self, n1, n2):
		self.n1 = n1
		self.n2 = n2

	def calculadora(self):
		self.soma = self.n1 + self.n2
		self.divisao = self.n1 / self.n2
		self.multiplicacao = self.n1 * self.n2
		self.subtracao = self.n1 - self.n2

if __name__ == '__main__':
	teste = Exercicio03b(eval(input("Digite o primeiro número: ")), eval(input("Digite o segundo número: ")))
	teste.calculadora()
	print("Soma: {} \nSubtração: {} \nMultiplicação: {} \nDivisão: {} \n".format(teste.soma, teste.subtracao, teste.multiplicacao, teste.divisao))