#Exercicio 22

class Ingresso(object):

	def __init__(self, preco, tipo):
		self.preco = float(preco)
		self.tipo = str(tipo)

	def imprimeValor(self):
		return self.preco

	def imprimeTipo(self):
		return self.tipo


class Vip(Ingresso):

	_adicional = 0.20

	def __init__(self, preco):
		super().__init__(self.reajuste(preco), self.tipo_ingresso())

	def reajuste(self, preco):
		return preco + (preco * self._adicional)

	def tipo_ingresso(self):
		return "Ingresso Vip"

class Normal(Ingresso):

	def __init__(self, preco):
		super().__init__(preco, self.tipo())

	def tipo(self):
		return "Ingresso Normal"

class CamaroteInferior(Vip):

	_localizacao = {1:"A", 2:"B", 3:"C"}

	def __init__(self, preco):
		super().__init__(preco)

	def verLocalizacao(self):
		return self._localizacao

class CamaroteSuperior(Vip):

	_localizacao = {1:"D", 2:"E", 3:"F"}

	def __init__(self, preco):
		super().__init__(self.reajuste(preco))

	def reajuste(self, preco):
		return preco + (preco * 0.3)

	def verLocalizacao(self):
		return self._localizacao

	def verPreco(self):
		return super().preco