class Exercicio11(object):

	def __init__(self, numero):
		self.numero = int(numero)

	def antecessor(self):
		if self.numero == 0:
			return "O antecessor do número Zero é 0 (Zero)."
		return "O antecessor é: {}".format(self.numero - 1)

if __name__ == '__main__':
	obj = Exercicio11(input("Digite um número: "))
	print(obj.antecessor())