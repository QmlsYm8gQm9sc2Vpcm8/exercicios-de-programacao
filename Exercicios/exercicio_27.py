class Exercicio27(object):

	def __init__(self, l1, l2):
		print(self.verificar_igualdade(l1,l2))
		print("Número de Ocorrencia L1: %i" % self.ocorrencia(l1))
		print("Número de Ocorrencia L2: %i" % self.ocorrencia(l2))

	def verificar_igualdade(self, l1, l2):
		if l1 == l2:
			return "São Iguais."
		return "São Diferentes."

	def ocorrencia(self, lista):
		return lista.count(lista[0])

if __name__ == '__main__':
	Exercicio27([1,2],[1,3])
