from abc import ABC, abstractmethod

class Funcionario(ABC):

	__nome = ""
	__idade = 0
	__salario = 5000

	@abstractmethod
	def aumenta_salario(self, valor):
		self.__salario += valor 

	@property
	def getsalario(self):
		return self.__salario

class Programador(Funcionario):

	def aumenta_salario(self):
		super().aumenta_salario(20)


class Analista(Funcionario):

	def aumenta_salario(self):
		super().aumenta_salario(30)

if __name__ == '__main__':
	programador = Programador()
	analista = Analista()
	programador.aumenta_salario()
	analista.aumenta_salario()
	print("Salário Programdor: {}".format(programador.getsalario))
	print("Salário Analista: {}".format(analista.getsalario))