class Exercicio15(object):

	def __init__(self):
		self.numeros = []

	def add_lista(self, numero):
		self.numeros.append(numero)

	def maior_num(self):
		return max(self.numeros)

	def verif_igualdade(self, numero):
		return numero in self.numeros

if __name__ == '__main__':
	obj = Exercicio15()
	i = 0
	while (i < 10):
		x = int(input("Digite um Número: "))
		if obj.verif_igualdade(x) == False:
			obj.add_lista(x)
			i += 1
		else:
			print("Digite um número diferente do que já digitou.")
	print(obj.maior_num())

