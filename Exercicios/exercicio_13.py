class Exercicio13(object):

	def __init__(self, homem, mulher):
		self.homem = homem
		self.mulher = mulher
		self.calculo = []
		self.ordenamento()
		self.calculo_idade()

	def ordenamento(self):
		self.homem.sort(reverse=True)
		self.mulher.sort()

	def calculo_idade(self):
		self.calculo.append(self.homem[0] + self.mulher[0])
		self.calculo.append(self.homem[1] * self.mulher[1])

	def resultado(self):
		return self.calculo.pop(0)

if __name__ == '__main__':
	homem = [int(input("Digite a idade do Primeiro Homem: ")), int(input("Digite a idade do Segundo Homem: "))]
	mulher = [int(input("Digite a idade da Primeira Mulher: ")),int(input("Digite a idade da Segunda Mulher:"))]
	obj = Exercicio13(homem,mulher)
	print("A soma das idades do homem mais velho com a mulher mais nova é %i" % obj.resultado())
	print("o produto das idades do homem mais novo com a mulher mais velha é %i" % obj.resultado())


