class Exercicio04b(object):

	def __init__(self, valor_pago, valor_produto, desconto):
		self.valor_pago = valor_pago
		self.valor_produto = valor_produto - desconto

	@property
	def troco(self):
		return self.valor_pago - self.valor_produto

if __name__ == '__main__':
	mercado = Exercicio04b(eval(input("Digite o valor pago: ")), eval(input("Digite o valor do produto: ")), eval(input("Digite o valor de desconto: ")))
	print("O troco a ser dado é: R$ %d " % mercado.troco)
