class Exercicio04(object):

	def __init__(self, valor_pago, valor_produto):
		self.valor_pago = valor_pago
		self.valor_produto = valor_produto

	@property
	def troco(self):
		return self.valor_produto - self.valor_pago

if __name__ == '__main__':
	mercado = Exercicio04(eval(input("Digite o valor pago: ")), eval(input("Digite o valor do produto: ")))
	print("O troco a ser dado é: %d " % mercado.troco)
