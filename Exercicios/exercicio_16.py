class Exercicio16(object):

	def __init__(self, comprimento, largura, altura):
		self.comprimento = comprimento
		self.largura = largura
		self.altura = altura

	def volume (self):
		volume = self.comprimento * self.altura * self.largura
		print("O volume é %s." % volume)

if __name__ == '__main__':
	obj = Exercicio16(eval(input("Digite o comprimento: ")), eval(input("Digite a largura: ")), eval(input("Digite a altura: "))).volume()
