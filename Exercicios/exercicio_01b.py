class Exercicio01b(object):
	
	idade = 0
	nome = ""
	nota = 0

	def maior_idade(self):
		if self.idade >= 18:
			return "É maior de idade"
		else: 
			return "Não é maior de idade"

	@property
	def media(self):
		return self.nota / 3
		
	def setnome (self, nome):
		self.nome = nome

	def setidade (self, idade):
		self.idade = int(idade)

	def calculo_nota(self, repeat):
		if repeat > 0:
			self.nota += int(input("Digite a nota do aluno: "))
			repeat -= 1
			self.calculo_nota(repeat)

	def aprovado(self):
		if (self.nota/3) >= 7:
			return "Aprovado"
		return "Reprovado"

if __name__ == "__main__":
	aluno = Exercicio01b()
	for i in range(5):
		aluno.setnome(input("Digite o nome do aluno: "))
		aluno.setidade(int(input("Digite a idade do aluno: ")))
		aluno.calculo_nota(3)
		print("Aluno: {}\nIdade: {}\nMédia: {}\nMaior de Idade?: {}\nAprovado?: {}\n\n".format(aluno.nome, aluno.idade, aluno.media, aluno.maior_idade(), aluno.aprovado()))
		aluno.nota = 0
			
