class Bicicleta(object):

	def quatidadeMarchas(self):
		print("Número de marchas de uma Bicicleta")

	def tipoFreio(self):
		print("Freio comum")

	def marca(self):
		print("Por trás de uma excelente bicicleta, sempre tem uma grande marca.")

class BicicletaPasseio(Bicicleta):

	def quantidadeMarchas(self):
		print("Possui Seis marchas (6).")

	def marca(self):
		print("AltaCycle City")

class BicicletaProfissional(Bicicleta):

	def quantidadeMarchas(self):
		print("Possui Vinte e Uma marchas (21).")

	def tipoFreio(self):
		print("Possui: Freio de qualidade? é AltaFreios.")

	def marca(self):
		print("AltaCycle Champion Speed")
		super().marca()

class Main():
	def main(self):
	 	bicicletas = [["Bicicleta de Passeio: ", BicicletaPasseio()], ["Bicicleta Profissional: ", BicicletaProfissional()]]
	 	for i in range(2):
	 		print()
	 		print(bicicletas[i][0])
	 		bicicletas[i][1].tipoFreio()
	 		bicicletas[i][1].marca()
	 		bicicletas[i][1].quantidadeMarchas()

if __name__ == '__main__':
	Main().main()
