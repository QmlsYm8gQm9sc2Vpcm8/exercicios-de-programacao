class Imovel(object):

	def __init__(self, preco, endereco):
		self.preco = preco
		self.endereco = endereco

class Novo(Imovel):

	def __init__(self, preco, endereco):
		self.adicional = 0.45
		super().__init__(self.preco_reajustado(preco), endereco)

	def preco_reajustado(self, preco):
		return preco + (preco * self.adicional)

	def porcentagem(self):
		return ("O valor adicional é de %i%" % (self.adicional*100))

class Velho(Imovel):

	def __init__(self, preco, endereco):
		self.desconto = 0.15
		super().__init__(self.preco_reajustado(preco), endereco)

	def preco_reajustado(self, preco):
		return preco - (preco * self.desconto)

	def porcentagem(self):
		return ("O valor de desconto é de %i%" % (self.desconto*100))

imovel = [Novo(45000, "Rua José"), Velho(60000, "Rua Maria")]
print(imovel[0].preco)
print(imovel[1].preco)
