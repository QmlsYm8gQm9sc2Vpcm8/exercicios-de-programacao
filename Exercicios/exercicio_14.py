class Exercicio14(object):

	def __init__(self, numero):
		self.numero = int(numero)

	def tabuada(self):
		for i in range(1, 11, 1):
			print("{} x {} = {}".format(self.numero, i, self.numero * i))

if __name__ == '__main__':
	tabuada = Exercicio14(8).tabuada()
