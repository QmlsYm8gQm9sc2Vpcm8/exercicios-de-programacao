class Exercicio02(object):

	def __init__(self, numero):
		self.numero = numero

	def numero_extenso(self):
		if self.numero == 1:
			return "Um"
		elif self.numero == 2:
			return "Dois"
		elif self.numero == 3:
			return "Três"
		elif self.numero == 4:
			return "Quatro"
		elif self.numero == 5:
			return "Cinco"
		else: 
			return "Número Inválido"

if __name__ == '__main__':
	numero = Exercicio02(eval(input("Digite um número: ")))
	print("O número por extenso é %s" % numero.numero_extenso())
