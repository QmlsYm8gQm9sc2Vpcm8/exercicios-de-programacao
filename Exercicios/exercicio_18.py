from abc import ABC, abstractmethod

class Animal(ABC):

	@abstractmethod
	def comer(self):
		return "O animal está comendo"

class Cachorro(Animal):

	def comer(self):
		print("{} Ração para Cachorro.".format(super().comer()))

class Gato(Animal):

	def comer(self):
		print("{} Ração para Gato.".format(super().comer()))

class Cavalo(Animal):

	def comer(self):
		print("{} Capim.".format(super().comer()))

class AnimalTeste(object):

	animais = [Cachorro(), Gato(), Cavalo()]

if __name__ == '__main__':	
	animal = AnimalTeste()
	for i in range(3):
		animal.animais[i].comer()