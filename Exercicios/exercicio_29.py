class Exercicio29(object):

	def __init__(self, lista):
		print(self.valor_proximo(self.calcular_media(lista), lista))

	def calcular_media(self, lista):
		return sum(lista)/len(lista)

	def valor_proximo(self, media, lista):
		valor = sum(lista)
		numero = 0
		for i in range(len(lista)):
			if (abs(media - lista[i])) <= valor:
				valor = abs(media-lista[i])
				numero = lista[i]
		return numero

if __name__ == '__main__':
	Exercicio29([2.5, 7.5, 10.0, 4.0])