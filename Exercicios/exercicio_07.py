class Exercicio07(object):

	def __init__(self):
		self.numero = []
		pass
				
	def receber_valor(self, numero):	
		self.numero.append(numero)

	@property
	def menor(self):
		return min(self.numero)

	@property
	def maior(self):
		return max(self.numero)

	@property
	def soma(self):
		return sum(self.numero)

	@property
	def media(self):
		return sum(self.numero)/len(self.numero)

if __name__ == '__main__':
	obj = Exercicio07()
	for i in range(10):
		obj.receber_valor(eval(input("Digite um número [%i]: " % i)))
	print("Menor número: {} \nMaior número: {} \nSoma: {} \nMédia: {} \n".format(obj.menor, obj.maior, obj.soma, obj.media))		
