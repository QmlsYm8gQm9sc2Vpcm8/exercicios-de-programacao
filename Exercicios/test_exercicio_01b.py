import unittest
from exercicio_01b import Exercicio01b

class TesteUnitarioExercicio01b(unittest.TestCase):

    #Constantes
    MAIOR_IDADE = 18
    MENOR_IDADE = 10
    NOTA = 15

    #Objeto Instanciado
    obj = Exercicio01b()
    self.obj.setidade(self.MAIOR_IDADE)
    self.obj.setnome("Josemar")
    
    def test_maior_idade(self):
        self.assertEqual(self.obj.maior_idade(), "É maior de idade")
        self.obj.setidade(self.MENOR_IDADE)
        self.assertEqual(self.obj.maior_idade(), "Não é maior de idade")

    def test_media(self):
    	self.assertEqual(self.obj.media, 5.0)

    def test_calculo_nota(self):



if __name__ == "__main__":
    unittest.main()
        
