# Exercicio 21

from abc import ABC, abstractmethod
class Exercicio21(ABC):

	def __init__(self, nome, idade, salario):
		self.nome = nome
		self.idade = idade
		self.salario = int(salario)

	@abstractmethod
	def aumentaSalario(self):
		pass

class Programador(Exercicio21):

	def aumentaSalario(self):
		self.salario += 20
		print("Programador: %s" % self.salario)

class Analista(Exercicio21):

	def aumentaSalario(self):
		self.salario += 30
		print("Analista: %s" % self.salario)

class Teste(object):

	def main(self):
		self.programador = Programador("Josemar", 19, 1900)
		self.analista = Analista("Luciano", 19, 1800)
		self.analista.aumentaSalario()
		self.programador.aumentaSalario()

if __name__ == '__main__':
 	Teste().main() 