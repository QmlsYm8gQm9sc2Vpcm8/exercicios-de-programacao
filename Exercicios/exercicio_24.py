class Exercicio24(object):

	def __init__(self, lista):
		print("Soma: %i" % self.soma(lista))
		print("Maior Elemento: %i" % self.maior_elemento(lista))
		print("Média: %i" % self.media(lista))
		print("Numero de Ocorrências, do primeiro número: %i" % self.ocorrencia(lista))

	def soma (self, lista):
		return sum(lista)

	def maior_elemento(self, lista):
		return max(lista)

	def media (self, lista):
		return self.soma(lista)/len(lista)

	def ocorrencia(self, lista):
		return lista.count(lista[0])

if __name__ == '__main__':
	Exercicio24([1, 25, 100, 1, 46, 99])