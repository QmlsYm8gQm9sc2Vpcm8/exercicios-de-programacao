class Exercicio12(object):

	def __init__(self, numero_1, numero_2):
		self.numero_1 = int(numero_1)
		self.numero_2 = int(numero_2)

	def maior_numero(self):
		if self.numero_1 > self.numero_2:
			return "O número {} é o maior.".format(self.numero_1)
		elif self.numero_2 > self.numero_1:
			return "O número {} é o maior.".format(self.numero_2)
		return "Os valores são iguais."

if __name__ == '__main__':
	obj = Exercicio12(input("Digite um número: "), input("Digite um número: "))
	print(obj.maior_numero())