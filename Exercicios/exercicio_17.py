class Exercicio17(object):

	__name = []

	def append_name(self, name):
		self.__name.append(name)

	def show_name(self):
		return self.__name.pop(0)

if __name__ == '__main__':
	obj = Exercicio17()
	for i in range(1, 6, 1):
		obj.append_name(input("Digite um nome: "))
	for i in range(1, 6, 1):
		print("Nome [%i]: %s" % (i, obj.show_name()))