class Exercicio28(object):

	alfabeto = {' ':0,"A":1,"B":2,"C":3,"D":4,"E":5,"F":6,"G":7,"H":8,"I":9,"J":10,"K":11,"L":12,"M":13,"N":14,
			  "O":15,"P":16,"Q":17,"R":18,"S":19,"T":20,"U":21,"V":22,"W":23,"X":24,"Y":25,"Z":26}
	numeracao = {0:' ',1:"A",2:"B",3:"C",4:"D",5:"E",6:"F",7:"G",8:"H",9:"I",10:"J",11:"K",12:"L",13:"M",14:"N",
			  15:"O",16:"P",17:"Q",18:"R",19:"S",20:"T",21:"U",22:"V",23:"W",24:"X",25:"Y",26:"Z"}

	codigo = ""
	mensagem = ""

	def codar(self, mensagem):
		for i in mensagem.upper():
			self.codigo += str(self.alfabeto[i])
			self.codigo += ' '
		print(self.codigo)
		self.codigo = ""
		self.mensagem = ""

	def decodar(self, codigo):
		self.codigo = ""
		codigo += " "
		for i in codigo:
			if i != ' ':
				self.codigo += i
			else:
				self.mensagem += self.numeracao[int(self.codigo)]
				self.codigo = ""

		print(self.mensagem)
		self.codigo = ""
		self.mensagem = ""


if __name__ == '__main__':
	Exercicio28().codar("Amanha tem aula")
	Exercicio28().decodar("1 13 1 14 8 1 0 20 5 13 0 1 21 12 1")

